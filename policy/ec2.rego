package main

deny[msg] {
    sg := input.module.security_group
    contains(sg.ingress_rules[_], "ssh-tcp") 
    msg = "SG defines access via ssh(22)"
}
